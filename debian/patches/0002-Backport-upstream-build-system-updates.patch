From: Felix Krull <f_krull@gmx.de>
Date: Sat, 26 Aug 2017 15:17:59 +0000
Subject: Backport upstream build system updates.

Origin: backport, https://bitbucket.org/heldercorreia/speedcrunch/src/2ae0cd8ee01142ad6de14d65ac003ce154ab486c/src/CMakeLists.txt
---
 src/CMakeLists.txt         | 209 ++++++++++++++++++---------------------------
 src/SpeedCrunchCPack.cmake |  72 ++++++++++++++++
 2 files changed, 155 insertions(+), 126 deletions(-)
 create mode 100644 src/SpeedCrunchCPack.cmake

diff --git a/src/CMakeLists.txt b/src/CMakeLists.txt
index 7be6aad..b0f62ee 100644
--- a/src/CMakeLists.txt
+++ b/src/CMakeLists.txt
@@ -1,52 +1,62 @@
-cmake_minimum_required(VERSION 2.8.11)
-
 project(speedcrunch)
+cmake_minimum_required(VERSION 3.0)
+
+set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")
+
+if (MSVC)
+    add_definitions(-D_USE_MATH_DEFINES)
+    add_definitions(-D_CRT_SECURE_NO_WARNINGS -D_CRT_NONSTDC_NO_WARNINGS -D_SCL_SECURE_NO_WARNINGS)
+else()
+    add_compile_options(-Wall
+                        -Wno-shift-negative-value)
+    set(CMAKE_CXX_FLAGS "-std=c++0x")
+endif()
+
+if(APPLE)
+    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
+endif()
+
 set(speedcrunch_VERSION "0.12")
 
 set(CMAKE_AUTOMOC ON)
+set(CMAKE_AUTORCC ON)
 set(CMAKE_COLOR_MAKEFILE ON)
 set(CMAKE_VERBOSE_MAKEFILE OFF)
 set(CMAKE_INCLUDE_CURRENT_DIR ON)
+set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
+set(CMAKE_MACOSX_RPATH ON)
 
-find_package(Qt5Core)
-find_package(Qt5Widgets)
-find_package(Qt5Help)
-
-if(APPLE)
-    set(PROGNAME SpeedCrunch)
-    set(MACOSX_BUNDLE_ICON_FILE speedcrunch.icns)
-    set(MACOSX_BUNDLE_SHORT_VERSION_STRING ${speedcrunch_VERSION})
-    set(MACOSX_BUNDLE_VERSION ${speedcrunch_VERSION})
-    set(MACOSX_BUNDLE_LONG_VERSION_STRING Version ${speedcrunch_VERSION})
-    set(CMAKE_OSX_ARCHITECTURES ppc;i386)
-else(APPLE)
-    set(PROGNAME speedcrunch)
-endif(APPLE)
+find_package(Qt5Core REQUIRED)
+find_package(Qt5Widgets REQUIRED)
+find_package(Qt5Help REQUIRED)
 
 ADD_DEFINITIONS("-DSPEEDCRUNCH_VERSION=\"${speedcrunch_VERSION}\"")
 ADD_DEFINITIONS(-DQT_USE_QSTRINGBUILDER)
 
 include(SourceFiles.cmake)
 
+if(APPLE)
+    set(APP_ICON_FILE speedcrunch.icns)
+    set(APP_ICON "${CMAKE_CURRENT_SOURCE_DIR}/resources/${APP_ICON_FILE}")
+    set(APPLICATION_NAME SpeedCrunch)
+    set(MACOSX_BUNDLE_ICON_FILE ${APP_ICON_FILE})
+    set(MACOSX_BUNDLE_SHORT_VERSION_STRING ${speedcrunch_VERSION})
+    set(MACOSX_BUNDLE_VERSION ${speedcrunch_VERSION})
+    set(MACOSX_BUNDLE_LONG_VERSION_STRING Version ${speedcrunch_VERSION})
+    set_source_files_properties(${APP_ICON} PROPERTIES MACOSX_PACKAGE_LOCATION "Resources")
+else()
+    set(APPLICATION_NAME speedcrunch)
+endif()
+
 if(WIN32)
     set(WIN32_RES_FILE ${CMAKE_CURRENT_BINARY_DIR}/speedcrunch.rc.obj)
     if(MINGW)
         add_custom_command(OUTPUT ${WIN32_RES_FILE}
                            COMMAND windres.exe ${CMAKE_CURRENT_SOURCE_DIR}/resources/speedcrunch.rc ${WIN32_RES_FILE})
-    else(MINGW)
+    else()
         add_custom_command(OUTPUT ${WIN32_RES_FILE}
                            COMMAND rc.exe /fo ${WIN32_RES_FILE} ${CMAKE_CURRENT_SOURCE_DIR}/resources/speedcrunch.rc)
-    endif(MINGW)
-endif(WIN32)
-
-string(COMPARE EQUAL "${CMAKE_CXX_COMPILER_ID}" MSVC USING_MSVC)
-if(USING_MSVC)
-    add_definitions(-D_USE_MATH_DEFINES)
-    add_definitions(-D_CRT_SECURE_NO_WARNINGS -D_CRT_NONSTDC_NO_WARNINGS -D_SCL_SECURE_NO_WARNINGS)
-else()
-    add_definitions(-Wall)
-    add_definitions(-Wno-shift-negative-value)
-    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
+    endif()
 endif()
 
 # Embedded manual
@@ -60,24 +70,24 @@ else()
     set(speedcrunch_RESOURCES ${speedcrunch_RESOURCES} ${CMAKE_CURRENT_SOURCE_DIR}/../doc/build_html_embedded/manual.qrc)
 endif()
 
-qt5_add_RESOURCES(speedcrunch_RESOURCES_SOURCES ${speedcrunch_RESOURCES})
-add_executable(${PROGNAME} WIN32 MACOSX_BUNDLE ${speedcrunch_SOURCES} ${speedcrunch_HEADERS_MOC} ${speedcrunch_RESOURCES_SOURCES} ${speedcrunch_FORMS_HEADERS} ${WIN32_RES_FILE})
+if(WIN32)
+    add_executable(${APPLICATION_NAME} WIN32 ${speedcrunch_SOURCES} ${speedcrunch_RESOURCES} ${WIN32_RES_FILE})
+elseif(APPLE)
+    add_executable(${APPLICATION_NAME} MACOSX_BUNDLE ${speedcrunch_SOURCES} ${speedcrunch_RESOURCES} ${APP_ICON})
+    set_target_properties(${APPLICATION_NAME} PROPERTIES MACOSX_BUNDLE_INFO_PLIST ${CMAKE_CURRENT_SOURCE_DIR}/../pkg/Info.plist)
+else()
+    add_executable(${APPLICATION_NAME} ${speedcrunch_SOURCES} ${speedcrunch_RESOURCES})
+endif()
 
 if(REBUILD_MANUAL)
-    add_dependencies(${PROGNAME} manual)
+    set_property(TARGET ${APPLICATION_NAME} PROPERTY AUTOGEN_TARGET_DEPENDS manual)
 endif()
 
-if(APPLE)
-    set( speedcrunch_RESOURCE_DIR ${CMAKE_CURRENT_BINARY_DIR}/${PROGNAME}.app/Contents/Resources )
-    add_custom_command(TARGET ${PROGNAME} POST_BUILD
-                       COMMAND mkdir ARGS -p ${speedcrunch_RESOURCE_DIR}
-                       COMMAND cp ARGS -f resources/${MACOSX_BUNDLE_ICON_FILE} ${speedcrunch_RESOURCE_DIR})
-endif(APPLE)
-
 add_custom_target(confclean COMMAND rm -rf Makefile CMakeFiles/ CMakeCache.txt cmake_install.cmake DartTestfile.txt install_manifest.txt)
 
 set(QT_LIBRARIES Qt5::Widgets Qt5::Help)
-target_link_libraries(${PROGNAME} ${QT_LIBRARIES})
+target_link_libraries(${APPLICATION_NAME} ${QT_LIBRARIES})
+qt5_use_modules(${APPLICATION_NAME} Widgets Help Sql)
 
 enable_testing()
 
@@ -104,113 +114,60 @@ add_executable(testser ${testser_SOURCES})
 target_link_libraries(testser ${QT_LIBRARIES})
 add_test(testser testser)
 
-set_property(TARGET ${PROGNAME} APPEND PROPERTY COMPILE_DEFINITIONS $<$<CONFIG:Debug>:EVALUATOR_DEBUG>)
+set_property(TARGET ${APPLICATION_NAME} APPEND PROPERTY COMPILE_DEFINITIONS $<$<CONFIG:Debug>:EVALUATOR_DEBUG>)
 
 include_directories(${CMAKE_BINARY_DIR} thirdparty core gui math)
 
 ################################# INSTALL ######################################
 
-if(NOT WIN32)
-    set(SHAREDIR "share/")
-    set(MENUDIR "${SHAREDIR}/applications/")
-    set(APPDATADIR "${SHAREDIR}/appdata/")
-    set(ICONDIR "${SHAREDIR}/pixmaps/")
-    set(BINDIR "bin")
-else(NOT WIN32)
-    set(BINDIR ".")
-    set(DATADIR ".")
-endif(NOT WIN32)
+if(UNIX AND NOT APPLE)
+    set(SHARE_DIR "share")
+    set(BIN_INSTALL_DIR "bin")
+
+    set(MENU_DIR "${SHARE_DIR}/applications/")
+    set(APP_DATA_DIR "${SHARE_DIR}/metainfo/")
+    set(ICON_DIR "${SHARE_DIR}/pixmaps/")
+elseif(APPLE)
+    set(BIN_INSTALL_DIR ".")
+    set(DOC_INSTALL_DIR ${APPLICATION_NAME}.app/Contents/Resources)
+else()
+    set(BIN_INSTALL_DIR ".")
+    set(DOC_INSTALL_DIR ".")
+endif()
 
 option(PORTABLE_SPEEDCRUNCH "Enable to build in portable mode" OFF)
 if(PORTABLE_SPEEDCRUNCH)
     add_definitions(-DSPEEDCRUNCH_PORTABLE)
 endif(PORTABLE_SPEEDCRUNCH)
 
-install(TARGETS ${PROGNAME} DESTINATION ${BINDIR})
+install(TARGETS ${APPLICATION_NAME} DESTINATION ${BIN_INSTALL_DIR})
 
-if(NOT WIN32)
-    install(FILES ../pkg/speedcrunch.desktop DESTINATION ${MENUDIR})
-    install(FILES resources/speedcrunch.png DESTINATION ${ICONDIR})
-    install(FILES ../pkg/speedcrunch.appdata.xml DESTINATION ${APPDATADIR})
-else(NOT WIN32)
-    install(FILES ../pkg/COPYING.rtf DESTINATION ${DATADIR})
-endif(NOT WIN32)
+if(UNIX AND NOT APPLE)
+    install(FILES ../pkg/speedcrunch.desktop DESTINATION ${MENU_DIR})
+    install(FILES resources/speedcrunch.png DESTINATION ${ICON_DIR})
+    install(FILES ../pkg/speedcrunch.appdata.xml DESTINATION ${APP_DATA_DIR})
+elseif(WIN32)
+    install(FILES ../pkg/COPYING.rtf DESTINATION ${DOC_INSTALL_DIR})
+endif()
 
 if(WIN32)
-    include(../pkg/QtWin32Deploy.cmake)
-    qtwin32_deploy_modules(${BINDIR} qtwin32_Qt5Widgets qtwin32_Qt5Help)
-    qtwin32_deploy_default_qt_conf(${BINDIR})
+    include(QtWin32Deploy)
+    qtwin32_deploy_modules(${BIN_INSTALL_DIR} qtwin32_Qt5Widgets qtwin32_Qt5Help)
+    qtwin32_deploy_default_qt_conf(${BIN_INSTALL_DIR})
     if(${qtwin32_QT_MINOR_VERSION} VERSION_LESS 5.5)
         # Qt 5.4 and below also require ICU.
-        qtwin32_deploy_modules(${BINDIR} qtwin32_ICU)
+        qtwin32_deploy_modules(${BIN_INSTALL_DIR} qtwin32_ICU)
     endif()
+elseif(APPLE)
+    include(MacdeployQt)
+    macdeployqt(${APPLICATION_NAME}.app ${APPLICATION_NAME})
 endif(WIN32)
-set(CMAKE_INSTALL_SYSTEM_RUNTIME_DESTINATION ${BINDIR})
-include(InstallRequiredSystemLibraries)
-
-################################# PACKAGE #####################################
 
-set(CPACK_PACKAGE_VENDOR SpeedCrunch)
-set(CPACK_PACKAGE_VERSION ${speedcrunch_VERSION})
-set(CPACK_PACKAGE_INSTALL_DIRECTORY SpeedCrunch)
-set(CPACK_PACKAGE_EXECUTABLES ${PROGNAME} SpeedCrunch)
-
-if(WIN32)
-    if(NOT CPACK_GENERATOR)
-        if(PORTABLE_SPEEDCRUNCH)
-            set(CPACK_GENERATOR ZIP)
-        else(PORTABLE_SPEEDCRUNCH)
-            set(CPACK_GENERATOR NSIS)
-        endif(PORTABLE_SPEEDCRUNCH)
-    endif(NOT CPACK_GENERATOR)
-
-    set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/../pkg/COPYING.rtf")
-    set(CPACK_NSIS_EXECUTABLES_DIRECTORY ${BINDIR})
-    set(CPACK_NSIS_INSTALLED_ICON_NAME "${BINDIR}\\\\${PROGNAME}.exe,0")
-    set(CPACK_NSIS_MUI_ICON "${CMAKE_CURRENT_SOURCE_DIR}\\\\resources\\\\speedcrunch.ico")
-    set(CPACK_NSIS_HELP_LINK "http://groups.google.com/group/speedcrunch")
-    set(CPACK_NSIS_URL_INFO_ABOUT "http://speedcrunch.org")
-    set(CPACK_NSIS_ENABLE_UNINSTALL_BEFORE_INSTALL true)
-
-    set(CPACK_NSIS_EXTRA_INSTALL_COMMANDS
-        "WriteRegStr SHCTX 'Software\\\\RegisteredApplications' 'SpeedCrunch' 'Software\\\\SpeedCrunch\\\\Capabilities'
-         WriteRegStr SHCTX 'Software\\\\SpeedCrunch\\\\Capabilities' 'ApplicationDescription' 'SpeedCrunch is a high-precision scientific calculator'
-         WriteRegStr SHCTX 'Software\\\\SpeedCrunch\\\\Capabilities' 'ApplicationName' 'SpeedCrunch'
-         WriteRegStr SHCTX 'Software\\\\SpeedCrunch\\\\Capabilities\\\\UrlAssociations' 'calculator' 'SpeedCrunch.Url.calculator'
-
-         WriteRegStr SHCTX 'Software\\\\Classes\\\\SpeedCrunch.Url.calculator' '' 'SpeedCrunch'
-         WriteRegStr SHCTX 'Software\\\\Classes\\\\SpeedCrunch.Url.calculator' 'FriendlyTypeName' 'SpeedCrunch'
-         WriteRegStr SHCTX 'Software\\\\Classes\\\\SpeedCrunch.Url.calculator\\\\shell\\\\open\\\\command' '' '\\\"$INSTDIR\\\\${BINDIR}\\\\${PROGNAME}.exe\\\"'
-         WriteRegStr SHCTX 'Software\\\\Classes\\\\SpeedCrunch.Url.calculator\\\\Application' 'ApplicationCompany' 'SpeedCrunch'
-
-         WriteRegStr SHCTX 'Software\\\\Classes\\\\Applications\\\\${PROGNAME}.exe' 'FriendlyAppName' 'SpeedCrunch'
-         ")
-    set(CPACK_NSIS_EXTRA_UNINSTALL_COMMANDS
-        "DeleteRegValue SHCTX 'Software\\\\RegisteredApplications' 'SpeedCrunch'
-         DeleteRegKey SHCTX 'Software\\\\SpeedCrunch'
-         DeleteRegKey SHCTX 'Software\\\\Classes\\\\SpeedCrunch.Url.calculator'
-         DeleteRegKey SHCTX 'Software\\\\Classes\\\\Applications\\\\${PROGNAME}.exe'
-         ")
-
-    if(${CMAKE_SIZEOF_VOID_P} EQUAL 8)
-        # When building a 64-bit installer, we add a check to make it fail on 32-bit systems
-        # because CPack doesn't do that by itself. Using this variable is probably a bit of a hack
-        # though.
-        set(CPACK_NSIS_DEFINES
-            "!include 'x64.nsh'
-             Function x64Check
-             \\\${IfNot} \\\${RunningX64}
-                 MessageBox MB_OK|MB_ICONEXCLAMATION 'This installer can only be run on 64-bit Windows.'
-                 Quit
-             \\\${EndIf}
-             FunctionEnd
-
-             Page custom x64Check
-            ")
-    endif()
-endif(WIN32)
+set(CMAKE_INSTALL_SYSTEM_RUNTIME_DESTINATION ${BIN_INSTALL_DIR})
+include(InstallRequiredSystemLibraries)
+set(CMAKE_INSTALL_UCRT_LIBRARIES TRUE)
 
-include(CPack)
+include(SpeedCrunchCPack.cmake)
 
 ################################ UNINSTALL #####################################
 
diff --git a/src/SpeedCrunchCPack.cmake b/src/SpeedCrunchCPack.cmake
new file mode 100644
index 0000000..e00ac2e
--- /dev/null
+++ b/src/SpeedCrunchCPack.cmake
@@ -0,0 +1,72 @@
+if(APPLE)
+    set(CMAKE_INSTALL_PREFIX "${CMAKE_BINARY_DIR}/install")
+endif()
+
+set(CPACK_PACKAGE_VENDOR SpeedCrunch)
+set(CPACK_PACKAGE_VERSION ${speedcrunch_VERSION})
+set(CPACK_PACKAGE_INSTALL_DIRECTORY SpeedCrunch)
+set(CPACK_PACKAGE_EXECUTABLES ${APPLICATION_NAME} SpeedCrunch)
+
+if(WIN32)
+    if(NOT CPACK_GENERATOR)
+        if(PORTABLE_SPEEDCRUNCH)
+            set(CPACK_GENERATOR ZIP)
+        else(PORTABLE_SPEEDCRUNCH)
+            set(CPACK_GENERATOR NSIS)
+        endif(PORTABLE_SPEEDCRUNCH)
+    endif(NOT CPACK_GENERATOR)
+
+    set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/../pkg/COPYING.rtf")
+    set(CPACK_NSIS_EXECUTABLES_DIRECTORY ${BIN_INSTALL_DIR})
+    set(CPACK_NSIS_INSTALLED_ICON_NAME "${BIN_INSTALL_DIR}\\\\${APPLICATION_NAME}.exe,0")
+    set(CPACK_NSIS_MUI_ICON "${CMAKE_CURRENT_SOURCE_DIR}\\\\resources\\\\speedcrunch.ico")
+    set(CPACK_NSIS_HELP_LINK "http://groups.google.com/group/speedcrunch")
+    set(CPACK_NSIS_URL_INFO_ABOUT "http://speedcrunch.org")
+    set(CPACK_NSIS_ENABLE_UNINSTALL_BEFORE_INSTALL true)
+
+    set(CPACK_NSIS_EXTRA_INSTALL_COMMANDS
+        "WriteRegStr SHCTX 'Software\\\\RegisteredApplications' 'SpeedCrunch' 'Software\\\\SpeedCrunch\\\\Capabilities'
+         WriteRegStr SHCTX 'Software\\\\SpeedCrunch\\\\Capabilities' 'ApplicationDescription' 'SpeedCrunch is a high-precision scientific calculator'
+         WriteRegStr SHCTX 'Software\\\\SpeedCrunch\\\\Capabilities' 'ApplicationName' 'SpeedCrunch'
+         WriteRegStr SHCTX 'Software\\\\SpeedCrunch\\\\Capabilities\\\\UrlAssociations' 'calculator' 'SpeedCrunch.Url.calculator'
+
+         WriteRegStr SHCTX 'Software\\\\Classes\\\\SpeedCrunch.Url.calculator' '' 'SpeedCrunch'
+         WriteRegStr SHCTX 'Software\\\\Classes\\\\SpeedCrunch.Url.calculator' 'FriendlyTypeName' 'SpeedCrunch'
+         WriteRegStr SHCTX 'Software\\\\Classes\\\\SpeedCrunch.Url.calculator\\\\shell\\\\open\\\\command' '' '\\\"$INSTDIR\\\\${BIN_INSTALL_DIR}\\\\${APPLICATION_NAME}.exe\\\"'
+         WriteRegStr SHCTX 'Software\\\\Classes\\\\SpeedCrunch.Url.calculator\\\\Application' 'ApplicationCompany' 'SpeedCrunch'
+
+         WriteRegStr SHCTX 'Software\\\\Classes\\\\Applications\\\\${APPLICATION_NAME}.exe' 'FriendlyAppName' 'SpeedCrunch'
+         ")
+    set(CPACK_NSIS_EXTRA_UNINSTALL_COMMANDS
+        "DeleteRegValue SHCTX 'Software\\\\RegisteredApplications' 'SpeedCrunch'
+         DeleteRegKey SHCTX 'Software\\\\SpeedCrunch'
+         DeleteRegKey SHCTX 'Software\\\\Classes\\\\SpeedCrunch.Url.calculator'
+         DeleteRegKey SHCTX 'Software\\\\Classes\\\\Applications\\\\${APPLICATION_NAME}.exe'
+         ")
+
+    if(${CMAKE_SIZEOF_VOID_P} EQUAL 8)
+        # When building a 64-bit installer, we add a check to make it fail on 32-bit systems
+        # because CPack doesn't do that by itself. Using this variable is probably a bit of a hack
+        # though.
+        set(CPACK_NSIS_DEFINES
+            "!include 'x64.nsh'
+             Function x64Check
+             \\\${IfNot} \\\${RunningX64}
+                 MessageBox MB_OK|MB_ICONEXCLAMATION 'This installer can only be run on 64-bit Windows.'
+                 Quit
+             \\\${EndIf}
+             FunctionEnd
+
+             Page custom x64Check
+            ")
+    endif()
+elseif(APPLE)
+     set(CPACK_GENERATOR "DragNDrop")
+     set(CPACK_DMG_FORMAT "UDBZ")
+     set(CPACK_DMG_VOLUME_NAME "${APPLICATION_NAME}")
+     set(CPACK_PACKAGE_FILE_NAME "${APPLICATION_NAME}")
+elseif(UNIX)
+    set(CPACK_SYSTEM_NAME "${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR}")
+endif()
+
+include(CPack)
